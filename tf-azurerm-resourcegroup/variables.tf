variable "keyvault_name" {
  type = string
  default = ""
}

variable "resourcegroup_name" {
  type = string
  default = ""  
}

variable "groups" {
  type = list(string)
  default = []
}

variable "azure_region" {
  type = string
}

variable "environment" {
  type = string
}

variable "project_name" {
  type = string
}

variable "project_contact" {
  type = string
}

variable "project_application_client_id" {
  type = string
}

variable "project_entity" {
  type = string
}

variable "tags" {
  type        = map
  default     = null
}