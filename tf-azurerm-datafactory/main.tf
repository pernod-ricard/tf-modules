locals {
  project_name = replace(var.project_name, " ", "")
  name = replace(var.name == "" ? local.project_name : var.name, " ", "")
}

data "azurerm_subscription" "current" {
}

/*
data "azuredevops_project" "p" {
  project_name = var.azure_devops_project_name
} 
*/

resource "azurerm_data_factory" "default" {
  name = lower("adf${var.project_entity}${local.name}${var.environment}")
  location = var.azure_region
  resource_group_name = var.resourcegroup_name

  identity{
    type= "SystemAssigned"
  }

  dynamic "vsts_configuration" {
    for_each = var.environment == "dev" ? [1] : []
    #for_each = var.azure_devops_enabled ? [1] : []
    content {
      account_name = regex("^https://dev.azure.com/([^/]*)/?$", var.azure_devops_org_service_url)[0]
      branch_name = "master"
      project_name = var.azure_devops_project_name
      repository_name = azuredevops_git_repository.default[0].name
      root_folder = "/"
      tenant_id = data.azurerm_subscription.current.tenant_id
    }
  }
  
  lifecycle {
    ignore_changes = [tags]
  }
}

resource "azurerm_key_vault_access_policy" "readonly" {
  key_vault_id = var.keyvault_id
  tenant_id = data.azurerm_subscription.current.tenant_id
  object_id = azurerm_data_factory.default.identity[0].principal_id
  secret_permissions = [
    "Get",
  ]
}

/*
- push the pre-post-deployment-script (used by the release pipeline)
*/
resource "azuredevops_git_repository" "default" {
  #count = var.azure_devops_enabled ? 1 : 0
  count = var.environment == "dev" ? 1 : 0
  project_id = var.azure_devops_project_id
  name       = "${var.azure_devops_project_name}.ADF"
  initialization {
    init_type = "Clean"
  }
  provisioner "local-exec" {
    interpreter = ["/bin/bash"]
    command = "./init-repo.sh"
    working_dir = path.module
    environment = {
      AZURE_DEVOPS_EXT_PAT = var.azure_devops_pat
      AZURE_DEVOPS_PROJECT_NAME = var.azure_devops_project_name
      AZURE_DEVOPS_ORG_SERVICE_URL = var.azure_devops_org_service_url
      AZURE_DEVOPS_REPO_NAME = "${var.azure_devops_project_name}.ADF"
      WEB_URL = azuredevops_git_repository.default[0].web_url
    }
  }
}