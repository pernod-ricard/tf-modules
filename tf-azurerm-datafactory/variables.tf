variable "name" {
  type = string
  default = ""
  description = "Override the default name of the data factory. By default it the name of the project."
}

variable "resourcegroup_name" {
  type = string
}

variable "keyvault_id" {
  type = string
}

/*
variable "azure_subscription_id" {
  type = string
}

variable "azure_tenant_id" {
  type = string
}
*/
variable "azure_region" {
  type = string
}

variable "project_name" {
  type = string
}

variable "project_application_client_id" {
  type = string
}

variable "project_entity" {
  type = string
}

variable "environment" {
  type = string
}

variable "azure_devops_org_service_url" {
  type = string
}

variable "azure_devops_project_name" {
  type = string
}

variable "azure_devops_project_id" {
  type = string
}

variable "azure_devops_pat" {
  type = string
}



