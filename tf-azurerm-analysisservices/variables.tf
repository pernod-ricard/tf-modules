variable "resourcegroup_name" {
  type = string
}

variable "project_name" {
  type = string
}

variable "project_entity" {
  type = string
}

variable "environment" {
  type = string
}

variable "azure_region" {
  type = string
}

variable "pricing_tier" {
  type = string
  default = "B1"
}

variable "allow_power_bi" {
  type = bool
  default = true
}