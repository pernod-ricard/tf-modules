variable "resourcegroup_name" {
  type = string
}

variable "azure_region" {
  type = string
}

variable "environment" {
  type = string
}

variable "project_name" {
  type = string
}

variable "project_entity" {
  type = string
}

variable "storage_name" {
  type = string
  default = ""
  description = "Override the default name of the blob storage. By default it the name of the project."
}