
variable "environment" {
  type = string
}

variable "project_name" {
  type = string
}

variable "project_entity" {
  type = string
}

variable "shared_elastic_pool" {
  type = string
  description = "The shared pool (located in <shared_resource_group> in which the db is located if not serveless"
  default = ""
}

variable "shared_server" {
  type = string
  description = "The shared MSSQL server in which the db is located"
}

variable "shared_resource_group" {
  type = string
  description = "The shared rg in which the <shared_sql_server> is located"
}

variable "min_capacity" {
  type = string
  default = "0.5"
}

variable "max_size_gb" {
  type = number
  default = 250
}

variable "max_capacity" {
  type = string
  default = "1"
  description = "Max number of vCores : accepted values : 1, 2, 4, 6, 8, 16 18 20, 24, 32, 40"
}

variable "auto_pause_delay_in_minutes" {
  type = string
  default = "60"
}

variable "azure_devops_project_id" {
  type = string
}

variable "azure_devops_project_name" {
  type = string
}

variable "azure_devops_pat" {
  type = string
}

variable "azure_devops_org_service_url" {
  type = string
}