locals { 
  serverless = "GP_S_Gen5_"
  project_name = replace(var.project_name, " ", "")
}

data "azurerm_sql_server" "server" {
  name                = var.shared_server
  resource_group_name = var.shared_resource_group
}

data "azurerm_mssql_elasticpool" "pool" {
  count = var.shared_elastic_pool != "" ? 1 : 0
  name                = var.shared_elastic_pool
  resource_group_name = var.shared_resource_group
  server_name         = var.shared_server 
}

resource "azuredevops_git_repository" "default" {
  count = var.environment == "dev" ? 1 : 0
  project_id = var.azure_devops_project_id
  name       = "${var.azure_devops_project_name}.Datatools"
  initialization {
    init_type = "Clean"
  }
  provisioner "local-exec" {
    interpreter = ["/bin/bash"]
    command = "./init-repo.sh"
    working_dir = path.module
    environment ={
      AZURE_DEVOPS_EXT_PAT = var.azure_devops_pat    
      AZURE_DEVOPS_PROJECT_NAME = var.azure_devops_project_name
      AZURE_DEVOPS_ORG_SERVICE_URL = var.azure_devops_org_service_url
      AZURE_DEVOPS_REPO_NAME = "${var.azure_devops_project_name}.DataTools"
    }
  }
}

resource "azurerm_mssql_database" "db" {
  name           = lower("sql${var.project_entity}${local.project_name}${var.environment}")
  server_id       = data.azurerm_sql_server.server.id
  collation       = "SQL_Latin1_General_CP1_CI_AS"
  license_type    = "LicenseIncluded"
  max_size_gb     = var.max_size_gb
  sku_name        = var.shared_elastic_pool == "" ? "${local.serverless}${var.max_capacity}" : "ElasticPool"
  elastic_pool_id = var.shared_elastic_pool == "" ? null : data.azurerm_mssql_elasticpool.pool[0].id
  min_capacity    = var.shared_elastic_pool == "" ? var.min_capacity : null
  auto_pause_delay_in_minutes = var.shared_elastic_pool == "" ? var.auto_pause_delay_in_minutes : null
  
  lifecycle {
    ignore_changes = [tags, license_type]
  }
}


