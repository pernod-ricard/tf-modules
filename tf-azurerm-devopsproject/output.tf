output "project_name" {
  #value = length(azuredevops_project.default) > 0 ? azuredevops_project.default[0].project_name : "${var.azure_devops_project_prefix}.${replace(var.azure_devops_project_name, " ", "")}"
  value = local.azure_devops_project_name
}

output "id" {
  #value = length(azuredevops_project.default) > 0 ? azuredevops_project.default[0].id : ""
  value = local.azure_devops_project_id
}