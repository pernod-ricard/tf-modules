locals {
  project_name = replace(var.project_name, " ", "")
  service_plan_name_cut = substr(local.project_name, 0, 40 - 3 - length(var.project_entity) - length(var.environment))
}

data "azurerm_app_service_plan" "default" {
  count = var.type_service_plan == "standard" ? 1 : 0
  name =  lower("pla${var.project_entity}${var.os}shared${var.resource_group_shared_number}${var.azure_region}${var.environment}")
  resource_group_name = lower("reg${var.project_entity}shared${var.resource_group_shared_number}${var.environment}")
}

data "azurerm_subscription" "current" {
}

resource "azurerm_app_service_plan" "consumption" {
  count = var.type_service_plan == "consumption" ? 1 : 0
  name                = lower("pla${var.project_entity}${local.service_plan_name_cut}${var.environment}")
  location            = var.azure_region
  resource_group_name = var.resourcegroup_name
  kind                = "FunctionApp"
  reserved = true
  sku {
    tier = "Dynamic"
    size = "Y1"
  }
  lifecycle {
    ignore_changes = [tags]
  }
}

locals {
  function_app_name = replace(var.function_app_name == "" ? local.project_name : var.function_app_name, " ", "")
}

# Different configuration between Standard and Consumption (serverless) plans
locals {
  app_service_plan_id = var.type_service_plan == "standard" ? data.azurerm_app_service_plan.default[0].id : azurerm_app_service_plan.consumption[0].id
  linux_fx_version    = var.type_service_plan == "standard" ? "DOCKER|mcr.microsoft.com/azure-functions/python:2.0-python3.6-appservice" : "PYTHON|3.6"
  always_on = var.type_service_plan == "standard" ? true : false
}

resource "azurerm_application_insights" "default" {
  application_type = "web"
  location = var.azure_region
  name = lower("ain${var.project_entity}${replace(local.function_app_name, " ", "")}${var.environment}")
  resource_group_name = var.resourcegroup_name

  lifecycle {
    ignore_changes = [tags]
  }
}

resource azurerm_function_app "default"{
  location = var.azure_region
  name = lower("fun${var.project_entity}${replace(local.function_app_name, " ", "")}${var.environment}")
  resource_group_name = var.resourcegroup_name
  app_service_plan_id = local.app_service_plan_id
  storage_connection_string = var.storage_account == null ? azurerm_storage_account.default[0].primary_connection_string : data.azurerm_storage_account.existing[0].primary_connection_string
  version = "~3"
  https_only = true
  os_type = lower(var.os)

  site_config {
    always_on = local.always_on
    linux_fx_version = local.linux_fx_version
  }
  app_settings = {
    FUNCTIONS_WORKER_RUNTIME = "python" 
    WEBSITE_NODE_DEFAULT_VERSION = "10.14.1"
    APPINSIGHTS_INSTRUMENTATIONKEY = azurerm_application_insights.default.instrumentation_key
  }
  lifecycle {
    ignore_changes = [app_settings, tags]
  }

  identity {
    type = "SystemAssigned"
  }
}

locals {
  storage_name = substr(local.function_app_name, 0, 24 - 3 - length(var.project_entity) - length(var.environment))
}

resource "azurerm_storage_account" "default" {  
  count = var.storage_account == null ? 1 : 0
  name = lower("sto${var.project_entity}${local.storage_name}${var.environment}")
  resource_group_name = var.resourcegroup_name
  enable_https_traffic_only=true
  location = var.azure_region
  account_tier = "Standard"
  account_replication_type = "LRS"
  account_kind = "StorageV2"

  lifecycle {
    ignore_changes = [tags]
  }
}

data "azurerm_storage_account" "existing" {
  count = var.storage_account == null ? 0 : 1
  name                = var.storage_account.name
  resource_group_name = var.resourcegroup_name
}

resource "azuredevops_git_repository" "default" {
  #count = var.azure_devops_enabled ? 1 : 0
  count = var.environment == "dev" ? 1 : 0
  project_id = var.azure_devops_project_id
  name       = "${var.azure_devops_project_name}.Function"
  initialization {
    init_type = "Clean"
  }
  provisioner "local-exec" {
    interpreter = ["/bin/bash"]
    command = "./init-repo.sh"
    working_dir = path.module
    environment = {
      AZURE_DEVOPS_EXT_PAT = var.azure_devops_pat
      AZURE_DEVOPS_PROJECT_NAME = var.azure_devops_project_name
      AZURE_DEVOPS_ORG_SERVICE_URL = var.azure_devops_org_service_url
      AZURE_DEVOPS_REPO_NAME = "${var.azure_devops_project_name}.Function"
      WEB_URL = azuredevops_git_repository.default[0].web_url
    }
  }
}

/*
resource "null_resource" "build_definition" {
  count = var.azure_devops_enabled ? 1 : 0

  provisioner "local-exec" {
    command = "./init-build.sh \"${var.azure_devops_org_service_url}\" \"${var.projectdevops.project_name}\" \"${module.devopsreposrepos.name}\""
    working_dir = path.module
    environment = {
      AZURE_DEVOPS_EXT_PAT = var.azure_devops_pat
    }
  }
}
*/

/*
data "external" "functionapp-identity" {
  count = 1
  program = ["bash", "${path.module}/get-identity.sh", azurerm_function_app.default.resource_group_name, azurerm_function_app.default.name]
}
*/

resource "azurerm_key_vault_access_policy" "readonly" {
  count = 1
  key_vault_id = var.keyvault_id
  tenant_id = data.azurerm_subscription.current.tenant_id
  object_id = azurerm_function_app.default.identity[0].principal_id
  #object_id = data.external.functionapp-identity[0].result["principalId"]
  secret_permissions = [
    "Get",
    "List"
  ]
}

/*
resource "null_resource" "release_definition" {
  count = var.project.application_client_id == "" ? 0 : 1
  provisioner "local-exec" {
    command = "./init-release.sh \"${var.azure_devops_org_service_url}\" \"${var.projectdevops.project_name}\" \"${module.devopsreposrepos.name}\" \"${azurerm_function_app.default.name}\" \"${var.project.application_client_id}\" \"${var.azure_subscription_id}\" \"${var.environment}\""
    working_dir = path.module
    environment = {
      AZURE_DEVOPS_EXT_PAT = var.azure_devops_pat
    }
  }
}
*/