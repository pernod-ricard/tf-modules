variable "resourcegroup_name" {
  type = string
}

variable "keyvault_id" {
  type = string
}

variable "function_app_name" {
  type = string
  default = ""
  description = "Override the default name of the function app. By default it the name of the project."
}

variable "type_service_plan" {
  type = string
  default = "standard"
  description = "Either standard or consumption"
}

variable "resource_group_shared_number" {
  type = string
  default = "01"
}

variable "storage_account" {
  type = object({
    name = string
    })
  default = null
}

variable "os" {
  type = string
}

variable "azure_region" {
  type = string
}

variable "project_name" {
  type = string
}

variable "project_application_client_id" {
  type = string
}

variable "project_entity" {
  type = string
}

variable "environment" {
  type = string
}

variable "azure_devops_org_service_url" {
  type = string
}

variable "azure_devops_pat" {
  type = string
}

variable "azure_devops_project_id" {
  type = string
}

variable "azure_devops_project_name" {
  type = string
}