variable "eventgridtopic_name" {
  type = string
  default = ""
  description = "Override the default name of the Event Grid Topic. By default it the name of the project."
}

variable "resourcegroup_name" {
  type = string
}

variable "azure_region" {
  type = string
}

variable "project_name" {
  type = string
}

variable "project_entity" {
  type = string
}

variable "environment" {
  type = string
}