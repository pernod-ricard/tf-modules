
locals {
  eventgridtopic_name = replace(var.eventgridtopic_name == "" ? var.project_name : var.eventgridtopic_name, " ", "")
}

resource "azurerm_eventgrid_topic" "default" {
  name                = lower("egt${var.project_entity}${local.eventgridtopic_name}${var.environment}")
  location            = var.azure_region
  resource_group_name = var.resourcegroup_name

  lifecycle {
    ignore_changes = [tags]
  }
}