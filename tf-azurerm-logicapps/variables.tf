/*
variable "approver" {
  type = string
  default = ""
  description = "Email or group of the approver in pipeline."
} 
*/

variable "resourcegroup_name" {
  type = string
}

variable "logicapps_names" {
  type = list(string)
  description = "List of logicapps to create without the convention naming."
}

/*
variable "azure_subscription_id" {
  type = string
}

variable "azure_tenant_id" {
  type = string
}
*/

variable "azure_region" {
  type = string
}

variable "environment" {
  type = string
}

variable "project_name" {
  type = string
}

variable "project_application_client_id" {
  type = string
}
variable "project_entity" {
  type = string
}

variable "azure_devops_project_name" {
  type = string
}

variable "azure_devops_org_service_url" {
  type = string
}

variable "azure_devops_project_id" {
  type = string
}

variable "azure_devops_pat" {
  type = string
}



