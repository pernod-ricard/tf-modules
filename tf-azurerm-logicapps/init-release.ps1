
if ($PSVersionTable.PSEdition -eq 'Desktop' -and (Get-Module -Name AzureRM -ListAvailable)) {
    Write-Warning -Message ('Az module not installed. Having both the AzureRM and ' +
      'Az modules installed at the same time is not supported.')
} else {
    Start-Job -Name Job1 -ScriptBlock {Install-Module -Name Az -Confirm:$False -Force -AllowClobber -Scope CurrentUser}
    Wait-Job -Name Job1
}

$serviceprincipalkey="62a40c2a-a441-474f-a606-b733161c1a51"
$artifact="$env:REPO_NAME CI"
$branch=$env:ENV

if ($branch -eq "prod") {
    $branch=master
}

az pipelines release definition list --project $env:AZURE_DEVOPS_PROJECT_NAME --org $env:AZURE_DEVOPS_ORG_SERVICE_URL --query "[?name=='[$env:ENV] Logicapps-$env:LOGICAPPS_NAME']"  -o tsv
$existing_pipeline=az pipelines release definition list --project $env:AZURE_DEVOPS_PROJECT_NAME --org $env:AZURE_DEVOPS_ORG_SERVICE_URL --query "[?name=='[$env:ENV] Logicapps-$env:LOGICAPPS_NAME']"  -o tsv
if ($existing_pipeline){
    Write-Error("Release pipeline '[$env:ENV] LogicApps-$env:LOGICAPPS_NAME' already exists. Creating release pipeline aborted.")
    exit 1
}

$subscription_name=az account show -s $env:AZURE_SUBSCRIPTION_ID --query "name" -o tsv
$tenant_id=az account show -s $env:AZURE_SUBSCRIPTION_ID --query "tenantId" -o tsv
$service_connection=az devops service-endpoint list --project $env:AZURE_DEVOPS_PROJECT_NAME --org $env:AZURE_DEVOPS_ORG_SERVICE_URL --query "[?authorization.parameters.serviceprincipalid=='$env:PROJECT_APPLICATION_CLIENT_ID'].id | [0]" -o tsv

if (!$service_connection) {
    Write-Host("No service connection ! Creating it")
    $env:AZURE_DEVOPS_EXT_AZURE_RM_SERVICE_PRINCIPAL_KEY=$serviceprincipalkey
    $service_connection=az devops service-endpoint azurerm create --project $env:AZURE_DEVOPS_PROJECT_NAME --org $env:AZURE_DEVOPS_ORG_SERVICE_URL --query "id" -o tsv --azure-rm-service-principal-id $env:PROJECT_APPLICATION_CLIENT_ID --azure-rm-subscription-id $env:AZURE_SUBSCRIPTION_ID --azure-rm-subscription-name $subscription_name --azure-rm-tenant-id $tenant_id --name "TEST ROMAIN Azure $subscription_name"
    Write-Host("Service connection created : $service_connection")
}
$artifact_id=az pipelines build definition list --project $env:AZURE_DEVOPS_PROJECT_NAME --org $env:AZURE_DEVOPS_ORG_SERVICE_URL --query "[?name=='$artifact'].id | [0]"
$queue_id=az pipelines queue list --project $env:AZURE_DEVOPS_PROJECT_NAME --org $env:AZURE_DEVOPS_ORG_SERVICE_URL --query "[?name=='Azure Pipelines'].id|[0]" -o tsv
$project_id=az devops project show --project $env:AZURE_DEVOPS_PROJECT_NAME --org $env:AZURE_DEVOPS_ORG_SERVICE_URL --query "id" -o tsv

$release_json=Get-Content -Path "./template-release.json" | ConvertFrom-JSON
$release_json.id="null"
$release_json.name="[$env:ENV] Logicapps-$env:LOGICAPPS_NAME"
$release_json.releaseNameFormat="[Logicapps][$env:ENV]" + ' Release-$(rev:r)'
$release_json.environments[0].deployPhases[0].deploymentInput.queueId=$queue_id
$release_json.environments[0].deployPhases[0].workflowTasks[0].name="Azure Deployment:" + $env:LOGICAPPS_NAME
$release_json.environments[0].deployPhases[0].workflowTasks[0].inputs.resourceGroupName=$env:RESOURCE_GROUP_NAME
$release_json.environments[0].deployPhases[0].workflowTasks[0].inputs.ConnectedServiceName=$service_connection
$release_json.environments[0].deployPhases[0].workflowTasks[0].inputs.csmFile='$(System.DefaultWorkingDirectory)/_' + "$artifact/Logicapps/$env:LOGICAPPS_NAME/$env:LOGICAPPS_NAME.json"
$release_json.environments[0].deployPhases[0].workflowTasks[0].inputs.csmParametersFile='$(System.DefaultWorkingDirectory)/_' + "$artifact/Logicapps/$env:LOGICAPPS_NAME/$env:ENV.parameters.json"
$release_json.artifacts[0].sourceId=$project_id + ":" + $artifact_id
$release_json.artifacts[0].definitionReference.definition.id=$artifact_id
$release_json.artifacts[0].definitionReference.project.id=$project_id
$release_json.artifacts[0].definitionReference.project.name=$env:AZURE_DEVOPS_PROJECT_NAME
$release_json.artifacts[0].definitionReference.artifactSourceDefinitionUrl.id="null"
$release_json.artifacts[0].alias="_$artifact"
$release_json.artifacts[0].definitionReference.defaultVersionBranch.id=$branch
$release_json.artifacts[0].definitionReference.defaultVersionBranch.name=$branch
$release_json.triggers[0].artifactAlias="_$artifact"
$release_json.triggers[0].triggerConditions[0].sourceBranch=$branch

$release_json | ConvertTo-Json -Depth 32 | set-content -Path "./release.json"

az devops invoke --org $env:AZURE_DEVOPS_ORG_SERVICE_URL  --area Release --resource definitions --route-parameters project=$env:AZURE_DEVOPS_PROJECT_NAME --http-method POST --in-file "./release.json" > response.json
