locals {
  project_name = replace(var.project_name, " ", "")
}

resource "azurerm_logic_app_workflow" "default" {
  count = length(var.logicapps_names)
  location = var.azure_region
  name = lower("lap${var.project_entity}${replace(var.project_name, " ", "")}${replace(var.logicapps_names[count.index], " ", "")}${var.environment}")
  resource_group_name = var.resourcegroup_name

  lifecycle {
    ignore_changes = [parameters, tags]
  }
}

resource "azuredevops_git_repository" "default" {
  count = var.environment == "dev" ? 1 : 0
  #count = var.azure_devops_enabled ? 1 : 0
  project_id = var.azure_devops_project_id
  name       = "${var.azure_devops_project_name}.LogicApps"
  initialization {
    init_type = "Clean"
  }
  provisioner "local-exec" {
    interpreter = ["/bin/bash"]
    working_dir = path.module
    command = "./init-repo.sh"
    environment = {
      AZURE_DEVOPS_EXT_PAT = var.azure_devops_pat
      AZURE_DEVOPS_PROJECT_NAME = var.azure_devops_project_name
      AZURE_DEVOPS_ORG_SERVICE_URL = var.azure_devops_org_service_url
      AZURE_DEVOPS_REPO_NAME = "${var.azure_devops_project_name}.LogicApps"
      WEB_URL = azuredevops_git_repository.default[0].web_url
    }
  }
}

/*
CANNOT BE USED SINCE THE AZ CLI IS NOT AVAILABLE
data "external" "logicapps-identity" {
  count = length(var.logicapps_names)
  program = ["bash", "${path.module}/get-identity.sh", azurerm_logic_app_workflow.default[count.index].resource_group_name, azurerm_logic_app_workflow.default[count.index].name, var.azure.subscription_id]
}
resource "azurerm_key_vault_access_policy" "readonly" {
  count = length(var.logicapps_names)
  key_vault_id = var.resource_group.key_vault.id
  tenant_id = var.azure.tenant_id
  object_id = data.external.logicapps-identity[count.index].result["principalId"]
  secret_permissions = [
    "Get",
    "List"
  ]
  depends_on = [azurerm_logic_app_workflow.default]
}
*/


/*
#RELEASE DEFINITIONS NEED TO BE CREATED MANUALLY
locals {
  logic_apps_name = var.project_application_client_id == "" ? [] : var.logicapps_names
}

resource "null_resource" "release_definition" {
  for_each = toset(local.logic_apps_name)
  triggers = {
    always_run = "${timestamp()}"
  }
  provisioner "local-exec" {
    interpreter = ["pwsh", "-File"]
    working_dir = path.module
    command = "./init-release.ps1"
    environment = {
      AZURE_DEVOPS_EXT_PAT = var.azure_devops_pat
      AZURE_DEVOPS_ORG_SERVICE_URL = var.azure_devops_org_service_url
      AZURE_DEVOPS_PROJECT_NAME = var.projectdevops.project_name
      REPO_NAME = azuredevops_git_repository.default[0].name
      RESOURCE_GROUP_NAME = var.resource_group.name 
      ENV = var.environment
      LOGICAPPS_NAME="${lower("lap${var.project_entity}${replace(var.project_name, " ", "")}${each.key}")}"
      AZURE_SUBSCRIPTION_ID = var.azure_subscription_id
      PROJECT_APPLICATION_CLIENT_ID = var.project_application_client_id
    }
  }
}
*/