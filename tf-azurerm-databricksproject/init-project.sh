#!/usr/bin/env bash
set -x

#GROUPNAME="$AZURE_DEVOPS_PROJECT_PREFIX.$PROJECT_NAME.Developers"
GROUPNAME="$AZURE_DEVOPS_PROJECT_NAME.Developers"


# Create Databricks project folder
curl -X POST -H "Authorization: Bearer ${DATABRICKS_TOKEN}" "${DATABRICKS_HOST}"/api/2.0/workspace/mkdirs -d '{"path":"'/$AZURE_DEVOPS_PROJECT_NAME'"}'
#curl -X POST -H "Authorization: Bearer ${DATABRICKS_TOKEN}" "${DATABRICKS_HOST}"/api/2.0/workspace/mkdirs -d '{"path":"'/$AZURE_DEVOPS_PROJECT_PREFIX.$PROJECT_NAME'"}'

# Create Databricks developers project group (fail silently if it already exists)
curl -X POST -H "Authorization: Bearer ${DATABRICKS_TOKEN}" "${DATABRICKS_HOST}"/api/2.0/groups/create -d '{"group_name":"'$GROUPNAME'"}'


if [[ "$ENV" == "dev" ]]
then
    # Create the global developers group (fail silently if it already exists)
    curl -X POST -H "Authorization: Bearer ${DATABRICKS_TOKEN}" "${DATABRICKS_HOST}"/api/2.0/groups/create -d '{"group_name":"developers"}'

    # Add the project developers group to the global developers group
    curl -X POST -H "Authorization: Bearer ${DATABRICKS_TOKEN}" "${DATABRICKS_HOST}"/api/2.0/groups/add-member -d '{"group_name":"'$GROUPNAME'","parent_name":"developers"}'
else
    GROUPNAMERUN="$AZURE_DEVOPS_PROJECT_NAME.Run"
    #GROUPNAMERUN="$AZURE_DEVOPS_PROJECT_PREFIX.$PROJECT_NAME.Run"
    # Create Databricks runners project group (fail silently if it already exists)
    curl -X POST -H "Authorization: Bearer ${DATABRICKS_TOKEN}" "${DATABRICKS_HOST}"/api/2.0/groups/create -d '{"group_name":"'$GROUPNAMERUN'"}'  

    # Create the global developers group (fail silently if it already exists)
    curl -X POST -H "Authorization: Bearer ${DATABRICKS_TOKEN}" "${DATABRICKS_HOST}"/api/2.0/groups/create -d '{"group_name":"runners"}'

    # Add the project runners group to the project developers group
    echo "Add $GROUPNAMERUN to $GROUPNAME"
    curl -X POST -H "Authorization: Bearer ${DATABRICKS_TOKEN}" "${DATABRICKS_HOST}"/api/2.0/groups/add-member -d '{"group_name":"'$GROUPNAMERUN'","parent_name":"'$GROUPNAME'"}'
    
    # Add the project runners group to the global runners group
    echo "Add $GROUPNAMERUN to runners"
    curl -X POST -H "Authorization: Bearer ${DATABRICKS_TOKEN}" "${DATABRICKS_HOST}"/api/2.0/groups/add-member -d '{"group_name":"'$GROUPNAMERUN'","parent_name":"runners"}'
fi

