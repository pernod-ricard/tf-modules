locals {
  project_name = replace(var.project_name, " ", "")
}

/*
- databricks folder
- databricks project group for devs (dev/staging/prod)
- databricks project group for runners (staging/prod)
*/
resource "null_resource" "databricksproject" {
  provisioner "local-exec" {
    interpreter = ["/bin/bash"]
    working_dir = path.module
    command = "./init-project.sh"
    environment = {
      DATABRICKS_HOST = var.databricks_host 
      DATABRICKS_TOKEN = var.databricks_token
      #PROJECT_NAME = local.project_name
      ENTITY = var.project_entity
      #AZURE_DEVOPS_PROJECT_PREFIX = var.azure_devops_project_prefix
      AZURE_DEVOPS_PROJECT_NAME = var.azure_devops_project_name
      ENV = var.environment
    }
  }
}


resource "azuredevops_git_repository" "default" {
  count = var.environment == "dev" ? 1 : 0
  project_id = var.azure_devops_project_id
  name       = "${var.azure_devops_project_name}.Databricks"
  initialization {
    init_type = "Clean"
  }
  provisioner "local-exec" {
    interpreter = ["/bin/bash"]
    command = "./init-repo.sh"
    working_dir = path.module
    environment ={
      AZURE_DEVOPS_EXT_PAT = var.azure_devops_pat    
      AZURE_DEVOPS_PROJECT_NAME = var.azure_devops_project_name
      AZURE_DEVOPS_ORG_SERVICE_URL = var.azure_devops_org_service_url
      AZURE_DEVOPS_REPO_NAME = "${var.azure_devops_project_name}.Databricks"
    }
  } 

}

/*
resource "null_resource" "databricksproject_service" {
  count = var.service_email == "" ? 0 : 1
  provisioner "local-exec" {
    command = "./init-project-service.sh \"${local.project_name}\" \"${var.project_entity}\" \"${var.azure_devops_project_prefix}\" \"${var.environment}\" \"${var.service_email}\""
    environment = {
      DATABRICKS_HOST = var.databricks_host
      DATABRICKS_TOKEN = var.databricks_token
    }
  }
  depends_on = [null_resource.databricksproject]
}
*/